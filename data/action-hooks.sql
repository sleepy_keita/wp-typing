insert into `wp_typing` values (null, 'activate_blog', 'action');
insert into `wp_typing` values (null, 'activate_header', 'action');
insert into `wp_typing` values (null, 'activate_plugin', 'action');
insert into `wp_typing` values (null, 'activate_wp_head', 'action');
insert into `wp_typing` values (null, 'activated_plugin', 'action');
insert into `wp_typing` values (null, 'activity_box_end', 'action');
insert into `wp_typing` values (null, 'add_admin_bar_menus', 'action');
insert into `wp_typing` values (null, 'add_attachment', 'action');
insert into `wp_typing` values (null, 'add_category_form_pre', 'action');
insert into `wp_typing` values (null, 'add_link', 'action');
insert into `wp_typing` values (null, 'add_link_category_form_pre', 'action');
insert into `wp_typing` values (null, 'add_meta_boxes', 'action');
insert into `wp_typing` values (null, 'add_meta_boxes_comment', 'action');
insert into `wp_typing` values (null, 'add_meta_boxes_link', 'action');
insert into `wp_typing` values (null, 'add_option', 'action');
insert into `wp_typing` values (null, 'add_option_{$option}', 'action');
insert into `wp_typing` values (null, 'add_site_option', 'action');
insert into `wp_typing` values (null, 'add_site_option_{$option}', 'action');
insert into `wp_typing` values (null, 'add_tag_form', 'action');
insert into `wp_typing` values (null, 'add_tag_form_fields', 'action');
insert into `wp_typing` values (null, 'add_tag_form_pre', 'action');
insert into `wp_typing` values (null, 'add_term_relationship', 'action');
insert into `wp_typing` values (null, 'add_user_to_blog', 'action');
insert into `wp_typing` values (null, 'add_{$meta_type}_meta', 'action');
insert into `wp_typing` values (null, 'added_existing_user', 'action');
insert into `wp_typing` values (null, 'added_option', 'action');
insert into `wp_typing` values (null, 'added_term_relationship', 'action');
insert into `wp_typing` values (null, 'added_usermeta', 'action');
insert into `wp_typing` values (null, 'added_{$meta_type}_meta', 'action');
insert into `wp_typing` values (null, 'admin_bar_init', 'action');
insert into `wp_typing` values (null, 'admin_color_scheme_picker', 'action');
insert into `wp_typing` values (null, 'admin_enqueue_scripts', 'action');
insert into `wp_typing` values (null, 'admin_footer', 'action');
insert into `wp_typing` values (null, 'admin_head', 'action');
insert into `wp_typing` values (null, 'admin_head-$hook_suffix', 'action');
insert into `wp_typing` values (null, 'admin_head-media-upload-popup', 'action');
insert into `wp_typing` values (null, 'admin_head_{$content_func}', 'action');
insert into `wp_typing` values (null, 'admin_init', 'action');
insert into `wp_typing` values (null, 'admin_menu', 'action');
insert into `wp_typing` values (null, 'admin_notices', 'action');
insert into `wp_typing` values (null, 'admin_page_access_denied', 'action');
insert into `wp_typing` values (null, 'admin_print_footer_scripts', 'action');
insert into `wp_typing` values (null, 'admin_print_scripts', 'action');
insert into `wp_typing` values (null, 'admin_print_scripts-$hook_suffix', 'action');
insert into `wp_typing` values (null, 'admin_print_scripts-media-upload-popup', 'action');
insert into `wp_typing` values (null, 'admin_print_styles', 'action');
insert into `wp_typing` values (null, 'admin_print_styles-$hook_suffix', 'action');
insert into `wp_typing` values (null, 'admin_print_styles-media-upload-popup', 'action');
insert into `wp_typing` values (null, 'admin_xml_ns', 'action');
insert into `wp_typing` values (null, 'adminmenu', 'action');
insert into `wp_typing` values (null, 'after_db_upgrade', 'action');
insert into `wp_typing` values (null, 'after_delete_post', 'action');
insert into `wp_typing` values (null, 'after_mu_upgrade', 'action');
insert into `wp_typing` values (null, 'after_plugin_row', 'action');
insert into `wp_typing` values (null, 'after_plugin_row_$plugin_file', 'action');
insert into `wp_typing` values (null, 'after_setup_theme', 'action');
insert into `wp_typing` values (null, 'after_signup_form', 'action');
insert into `wp_typing` values (null, 'after_switch_theme', 'action');
insert into `wp_typing` values (null, 'after_theme_row', 'action');
insert into `wp_typing` values (null, 'after_theme_row_$stylesheet', 'action');
insert into `wp_typing` values (null, 'after_wp_tiny_mce', 'action');
insert into `wp_typing` values (null, 'akismet_comment_check_response', 'action');
insert into `wp_typing` values (null, 'akismet_spam_caught', 'action');
insert into `wp_typing` values (null, 'akismet_submit_nonspam_comment', 'action');
insert into `wp_typing` values (null, 'akismet_submit_spam_comment', 'action');
insert into `wp_typing` values (null, 'akismet_tabs', 'action');
insert into `wp_typing` values (null, 'all_admin_notices', 'action');
insert into `wp_typing` values (null, 'archive_blog', 'action');
insert into `wp_typing` values (null, 'atom_author', 'action');
insert into `wp_typing` values (null, 'atom_comments_ns', 'action');
insert into `wp_typing` values (null, 'atom_entry', 'action');
insert into `wp_typing` values (null, 'atom_head', 'action');
insert into `wp_typing` values (null, 'atom_ns', 'action');
insert into `wp_typing` values (null, 'attachment_submitbox_misc_actions', 'action');
insert into `wp_typing` values (null, 'auth_cookie_bad_hash', 'action');
insert into `wp_typing` values (null, 'auth_cookie_bad_username', 'action');
insert into `wp_typing` values (null, 'auth_cookie_expired', 'action');
insert into `wp_typing` values (null, 'auth_cookie_malformed', 'action');
insert into `wp_typing` values (null, 'auth_cookie_valid', 'action');
insert into `wp_typing` values (null, 'auth_redirect', 'action');
insert into `wp_typing` values (null, 'before_delete_post', 'action');
insert into `wp_typing` values (null, 'before_signup_form', 'action');
insert into `wp_typing` values (null, 'before_wp_tiny_mce', 'action');
insert into `wp_typing` values (null, 'begin_fetch_post_thumbnail_html', 'action');
insert into `wp_typing` values (null, 'blog_privacy_selector', 'action');
insert into `wp_typing` values (null, 'check_admin_referer', 'action');
insert into `wp_typing` values (null, 'check_ajax_referer', 'action');
insert into `wp_typing` values (null, 'check_comment_flood', 'action');
insert into `wp_typing` values (null, 'clean_attachment_cache', 'action');
insert into `wp_typing` values (null, 'clean_object_term_cache', 'action');
insert into `wp_typing` values (null, 'clean_page_cache', 'action');
insert into `wp_typing` values (null, 'clean_post_cache', 'action');
insert into `wp_typing` values (null, 'clean_term_cache', 'action');
insert into `wp_typing` values (null, 'clear_auth_cookie', 'action');
insert into `wp_typing` values (null, 'comment_add_author_url', 'action');
insert into `wp_typing` values (null, 'comment_atom_entry', 'action');
insert into `wp_typing` values (null, 'comment_closed', 'action');
insert into `wp_typing` values (null, 'comment_duplicate_trigger', 'action');
insert into `wp_typing` values (null, 'comment_flood_trigger', 'action');
insert into `wp_typing` values (null, 'comment_form', 'action');
insert into `wp_typing` values (null, 'comment_form_after', 'action');
insert into `wp_typing` values (null, 'comment_form_after_fields', 'action');
insert into `wp_typing` values (null, 'comment_form_before', 'action');
insert into `wp_typing` values (null, 'comment_form_before_fields', 'action');
insert into `wp_typing` values (null, 'comment_form_comments_closed', 'action');
insert into `wp_typing` values (null, 'comment_form_logged_in_after', 'action');
insert into `wp_typing` values (null, 'comment_form_must_log_in_after', 'action');
insert into `wp_typing` values (null, 'comment_form_top', 'action');
insert into `wp_typing` values (null, 'comment_id_not_found', 'action');
insert into `wp_typing` values (null, 'comment_loop_start', 'action');
insert into `wp_typing` values (null, 'comment_on_draft', 'action');
insert into `wp_typing` values (null, 'comment_on_password_protected', 'action');
insert into `wp_typing` values (null, 'comment_on_trash', 'action');
insert into `wp_typing` values (null, 'comment_post', 'action');
insert into `wp_typing` values (null, 'comment_remove_author_url', 'action');
insert into `wp_typing` values (null, 'comment_{$new_status}_{$comment->comment_type}', 'action');
insert into `wp_typing` values (null, 'comment_{$old_status}_to_{$new_status}', 'action');
insert into `wp_typing` values (null, 'commentrss2_item', 'action');
insert into `wp_typing` values (null, 'comments_atom_head', 'action');
insert into `wp_typing` values (null, 'commentsrss2_head', 'action');
insert into `wp_typing` values (null, 'core_upgrade_preamble', 'action');
insert into `wp_typing` values (null, 'create_$taxonomy', 'action');
insert into `wp_typing` values (null, 'create_term', 'action');
insert into `wp_typing` values (null, 'created_$taxonomy', 'action');
insert into `wp_typing` values (null, 'created_term', 'action');
insert into `wp_typing` values (null, 'current_screen', 'action');
insert into `wp_typing` values (null, 'custom_header_options', 'action');
insert into `wp_typing` values (null, 'customize_controls_enqueue_scripts', 'action');
insert into `wp_typing` values (null, 'customize_controls_init', 'action');
insert into `wp_typing` values (null, 'customize_controls_print_footer_scripts', 'action');
insert into `wp_typing` values (null, 'customize_controls_print_scripts', 'action');
insert into `wp_typing` values (null, 'customize_controls_print_styles', 'action');
insert into `wp_typing` values (null, 'customize_preview_init', 'action');
insert into `wp_typing` values (null, 'customize_register', 'action');
insert into `wp_typing` values (null, 'customize_render_control', 'action');
insert into `wp_typing` values (null, 'customize_render_section', 'action');
insert into `wp_typing` values (null, 'customize_save', 'action');
insert into `wp_typing` values (null, 'dbx_post_advanced', 'action');
insert into `wp_typing` values (null, 'dbx_post_sidebar', 'action');
insert into `wp_typing` values (null, 'deactivate_blog', 'action');
insert into `wp_typing` values (null, 'deactivate_plugin', 'action');
insert into `wp_typing` values (null, 'deactivated_plugin', 'action');
insert into `wp_typing` values (null, 'delete_$taxonomy', 'action');
insert into `wp_typing` values (null, 'delete_attachment', 'action');
insert into `wp_typing` values (null, 'delete_blog', 'action');
insert into `wp_typing` values (null, 'delete_comment', 'action');
insert into `wp_typing` values (null, 'delete_link', 'action');
insert into `wp_typing` values (null, 'delete_option', 'action');
insert into `wp_typing` values (null, 'delete_option_$option', 'action');
insert into `wp_typing` values (null, 'delete_post', 'action');
insert into `wp_typing` values (null, 'delete_postmeta', 'action');
insert into `wp_typing` values (null, 'delete_site_option', 'action');
insert into `wp_typing` values (null, 'delete_site_option_{$option}', 'action');
insert into `wp_typing` values (null, 'delete_term', 'action');
insert into `wp_typing` values (null, 'delete_term_relationships', 'action');
insert into `wp_typing` values (null, 'delete_term_taxonomy', 'action');
insert into `wp_typing` values (null, 'delete_user', 'action');
insert into `wp_typing` values (null, 'delete_usermeta', 'action');
insert into `wp_typing` values (null, 'delete_{$meta_type}_meta', 'action');
insert into `wp_typing` values (null, 'delete_{$meta_type}meta', 'action');
insert into `wp_typing` values (null, 'deleted_comment', 'action');
insert into `wp_typing` values (null, 'deleted_link', 'action');
insert into `wp_typing` values (null, 'deleted_option', 'action');
insert into `wp_typing` values (null, 'deleted_post', 'action');
insert into `wp_typing` values (null, 'deleted_postmeta', 'action');
insert into `wp_typing` values (null, 'deleted_site_transient', 'action');
insert into `wp_typing` values (null, 'deleted_term_relationships', 'action');
insert into `wp_typing` values (null, 'deleted_term_taxonomy', 'action');
insert into `wp_typing` values (null, 'deleted_transient', 'action');
insert into `wp_typing` values (null, 'deleted_user', 'action');
insert into `wp_typing` values (null, 'deleted_usermeta', 'action');
insert into `wp_typing` values (null, 'deleted_{$meta_type}_meta', 'action');
insert into `wp_typing` values (null, 'deleted_{$meta_type}meta', 'action');
insert into `wp_typing` values (null, 'deprecated_argument_run', 'action');
insert into `wp_typing` values (null, 'deprecated_file_included', 'action');
insert into `wp_typing` values (null, 'deprecated_function_run', 'action');
insert into `wp_typing` values (null, 'do_action', 'action');
insert into `wp_typing` values (null, 'do_actionbelow', 'action');
insert into `wp_typing` values (null, 'do_meta_boxes', 'action');
insert into `wp_typing` values (null, 'do_robots', 'action');
insert into `wp_typing` values (null, 'do_robotstxt', 'action');
insert into `wp_typing` values (null, 'doing_it_wrong_run', 'action');
insert into `wp_typing` values (null, 'dynamic_sidebar', 'action');
insert into `wp_typing` values (null, 'edit_$taxonomy', 'action');
insert into `wp_typing` values (null, 'edit_attachment', 'action');
insert into `wp_typing` values (null, 'edit_category_form', 'action');
insert into `wp_typing` values (null, 'edit_category_form_fields', 'action');
insert into `wp_typing` values (null, 'edit_category_form_pre', 'action');
insert into `wp_typing` values (null, 'edit_comment', 'action');
insert into `wp_typing` values (null, 'edit_form_advanced', 'action');
insert into `wp_typing` values (null, 'edit_form_after_editor', 'action');
insert into `wp_typing` values (null, 'edit_form_after_title', 'action');
insert into `wp_typing` values (null, 'edit_link', 'action');
insert into `wp_typing` values (null, 'edit_link_category_form', 'action');
insert into `wp_typing` values (null, 'edit_link_category_form_fields', 'action');
insert into `wp_typing` values (null, 'edit_link_category_form_pre', 'action');
insert into `wp_typing` values (null, 'edit_page_form', 'action');
insert into `wp_typing` values (null, 'edit_post', 'action');
insert into `wp_typing` values (null, 'edit_tag_form', 'action');
insert into `wp_typing` values (null, 'edit_tag_form_fields', 'action');
insert into `wp_typing` values (null, 'edit_tag_form_pre', 'action');
insert into `wp_typing` values (null, 'edit_term', 'action');
insert into `wp_typing` values (null, 'edit_term_taxonomies', 'action');
insert into `wp_typing` values (null, 'edit_term_taxonomy', 'action');
insert into `wp_typing` values (null, 'edit_terms', 'action');
insert into `wp_typing` values (null, 'edit_user_profile', 'action');
insert into `wp_typing` values (null, 'edit_user_profile_update', 'action');
insert into `wp_typing` values (null, 'edited_$taxonomy', 'action');
insert into `wp_typing` values (null, 'edited_term', 'action');
insert into `wp_typing` values (null, 'edited_term_taxonomies', 'action');
insert into `wp_typing` values (null, 'edited_term_taxonomy', 'action');
insert into `wp_typing` values (null, 'edited_terms', 'action');
insert into `wp_typing` values (null, 'end_fetch_post_thumbnail_html', 'action');
insert into `wp_typing` values (null, 'export_filters', 'action');
insert into `wp_typing` values (null, 'export_wp', 'action');
insert into `wp_typing` values (null, 'get_footer', 'action');
insert into `wp_typing` values (null, 'get_header', 'action');
insert into `wp_typing` values (null, 'get_search_form', 'action');
insert into `wp_typing` values (null, 'get_sidebar', 'action');
insert into `wp_typing` values (null, 'get_template_part_{$slug}', 'action');
insert into `wp_typing` values (null, 'grant_super_admin', 'action');
insert into `wp_typing` values (null, 'granted_super_admin', 'action');
insert into `wp_typing` values (null, 'http_api_debug', 'action');
insert into `wp_typing` values (null, 'in_admin_footer', 'action');
insert into `wp_typing` values (null, 'in_admin_header', 'action');
insert into `wp_typing` values (null, 'in_plugin_update_message-$file', 'action');
insert into `wp_typing` values (null, 'in_theme_update_message-$theme_key', 'action');
insert into `wp_typing` values (null, 'init', 'action');
insert into `wp_typing` values (null, 'install_plugins_table_header', 'action');
insert into `wp_typing` values (null, 'install_themes_table_header', 'action');
insert into `wp_typing` values (null, 'load-$pagenow', 'action');
insert into `wp_typing` values (null, 'load-categories.php', 'action');
insert into `wp_typing` values (null, 'load-edit-link-categories.php', 'action');
insert into `wp_typing` values (null, 'load-page-new.php', 'action');
insert into `wp_typing` values (null, 'load-page.php', 'action');
insert into `wp_typing` values (null, 'load-widgets.php', 'action');
insert into `wp_typing` values (null, 'load_feed_engine', 'action');
insert into `wp_typing` values (null, 'load_textdomain', 'action');
insert into `wp_typing` values (null, 'login_enqueue_scripts', 'action');
insert into `wp_typing` values (null, 'login_footer', 'action');
insert into `wp_typing` values (null, 'login_form', 'action');
insert into `wp_typing` values (null, 'login_head', 'action');
insert into `wp_typing` values (null, 'login_init', 'action');
insert into `wp_typing` values (null, 'lost_password', 'action');
insert into `wp_typing` values (null, 'lostpassword_form', 'action');
insert into `wp_typing` values (null, 'lostpassword_post', 'action');
insert into `wp_typing` values (null, 'make_delete_blog', 'action');
insert into `wp_typing` values (null, 'make_ham_blog', 'action');
insert into `wp_typing` values (null, 'make_ham_user', 'action');
insert into `wp_typing` values (null, 'make_spam_blog', 'action');
insert into `wp_typing` values (null, 'make_spam_user', 'action');
insert into `wp_typing` values (null, 'make_undelete_blog', 'action');
insert into `wp_typing` values (null, 'manage_comments_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_comments_nav', 'action');
insert into `wp_typing` values (null, 'manage_link_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_media_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_pages_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_plugins_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_posts_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_sites_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_themes_custom_column', 'action');
insert into `wp_typing` values (null, 'manage_{$post->post_type}_posts_custom_column', 'action');
insert into `wp_typing` values (null, 'mature_blog', 'action');
insert into `wp_typing` values (null, 'media_buttons', 'action');
insert into `wp_typing` values (null, 'media_upload_$tab', 'action');
insert into `wp_typing` values (null, 'media_upload_$type', 'action');
insert into `wp_typing` values (null, 'mu_activity_box_end', 'action');
insert into `wp_typing` values (null, 'mu_rightnow_end', 'action');
insert into `wp_typing` values (null, 'muplugins_loaded', 'action');
insert into `wp_typing` values (null, 'myblogs_allblogs_options', 'action');
insert into `wp_typing` values (null, 'network_admin_menu', 'action');
insert into `wp_typing` values (null, 'network_admin_notices', 'action');
insert into `wp_typing` values (null, 'network_site_users_after_list_table', 'action');
insert into `wp_typing` values (null, 'opml_head', 'action');
insert into `wp_typing` values (null, 'password_reset', 'action');
insert into `wp_typing` values (null, 'permalink_structure_changed', 'action');
insert into `wp_typing` values (null, 'personal_options', 'action');
insert into `wp_typing` values (null, 'personal_options_update', 'action');
insert into `wp_typing` values (null, 'pingback_post', 'action');
insert into `wp_typing` values (null, 'plugins_loaded', 'action');
insert into `wp_typing` values (null, 'populate_options', 'action');
insert into `wp_typing` values (null, 'post-html-upload-ui', 'action');
insert into `wp_typing` values (null, 'post-plupload-upload-ui', 'action');
insert into `wp_typing` values (null, 'post-upload-ui', 'action');
insert into `wp_typing` values (null, 'post_comment_status_meta_box-options', 'action');
insert into `wp_typing` values (null, 'post_edit_form_tag', 'action');
insert into `wp_typing` values (null, 'post_submitbox_misc_actions', 'action');
insert into `wp_typing` values (null, 'post_submitbox_start', 'action');
insert into `wp_typing` values (null, 'post_updated', 'action');
insert into `wp_typing` values (null, 'posts_selection', 'action');
insert into `wp_typing` values (null, 'pre-html-upload-ui', 'action');
insert into `wp_typing` values (null, 'pre-plupload-upload-ui', 'action');
insert into `wp_typing` values (null, 'pre-upload-ui', 'action');
insert into `wp_typing` values (null, 'pre_comment_on_post', 'action');
insert into `wp_typing` values (null, 'pre_current_active_plugins', 'action');
insert into `wp_typing` values (null, 'pre_post_update', 'action');
insert into `wp_typing` values (null, 'preprocess_signup_form', 'action');
insert into `wp_typing` values (null, 'print_media_templates', 'action');
insert into `wp_typing` values (null, 'private_to_published', 'action');
insert into `wp_typing` values (null, 'profile_personal_options', 'action');
insert into `wp_typing` values (null, 'profile_update', 'action');
insert into `wp_typing` values (null, 'publish_phone', 'action');
insert into `wp_typing` values (null, 'quick_edit_custom_box', 'action');
insert into `wp_typing` values (null, 'rdf_header', 'action');
insert into `wp_typing` values (null, 'rdf_item', 'action');
insert into `wp_typing` values (null, 'rdf_ns', 'action');
insert into `wp_typing` values (null, 'refresh_blog_details', 'action');
insert into `wp_typing` values (null, 'register_form', 'action');
insert into `wp_typing` values (null, 'register_post', 'action');
insert into `wp_typing` values (null, 'register_sidebar', 'action');
insert into `wp_typing` values (null, 'registered_post_type', 'action');
insert into `wp_typing` values (null, 'registered_taxonomy', 'action');
insert into `wp_typing` values (null, 'remove_user_from_blog', 'action');
insert into `wp_typing` values (null, 'restrict_manage_comments', 'action');
insert into `wp_typing` values (null, 'restrict_manage_posts', 'action');
insert into `wp_typing` values (null, 'restrict_manage_users', 'action');
insert into `wp_typing` values (null, 'retreive_password', 'action');
insert into `wp_typing` values (null, 'retrieve_password', 'action');
insert into `wp_typing` values (null, 'retrieve_password_key', 'action');
insert into `wp_typing` values (null, 'revoke_super_admin', 'action');
insert into `wp_typing` values (null, 'revoked_super_admin', 'action');
insert into `wp_typing` values (null, 'right_now_content_table_end', 'action');
insert into `wp_typing` values (null, 'right_now_discussion_table_end', 'action');
insert into `wp_typing` values (null, 'right_now_table_end', 'action');
insert into `wp_typing` values (null, 'rightnow_end', 'action');
insert into `wp_typing` values (null, 'rss2_comments_ns', 'action');
insert into `wp_typing` values (null, 'rss2_head', 'action');
insert into `wp_typing` values (null, 'rss2_item', 'action');
insert into `wp_typing` values (null, 'rss2_ns', 'action');
insert into `wp_typing` values (null, 'rss_head', 'action');
insert into `wp_typing` values (null, 'rss_item', 'action');
insert into `wp_typing` values (null, 'sanitize_comment_cookies', 'action');
insert into `wp_typing` values (null, 'save_post', 'action');
insert into `wp_typing` values (null, 'set_auth_cookie', 'action');
insert into `wp_typing` values (null, 'set_comment_cookies', 'action');
insert into `wp_typing` values (null, 'set_current_user', 'action');
insert into `wp_typing` values (null, 'set_logged_in_cookie', 'action');
insert into `wp_typing` values (null, 'set_object_terms', 'action');
insert into `wp_typing` values (null, 'set_user_role', 'action');
insert into `wp_typing` values (null, 'setted_site_transient', 'action');
insert into `wp_typing` values (null, 'setted_transient', 'action');
insert into `wp_typing` values (null, 'setup_theme', 'action');
insert into `wp_typing` values (null, 'show_user_profile', 'action');
insert into `wp_typing` values (null, 'shutdown', 'action');
insert into `wp_typing` values (null, 'sidebar_admin_page', 'action');
insert into `wp_typing` values (null, 'sidebar_admin_setup', 'action');
insert into `wp_typing` values (null, 'signup_blogform', 'action');
insert into `wp_typing` values (null, 'signup_extra_fields', 'action');
insert into `wp_typing` values (null, 'signup_finished', 'action');
insert into `wp_typing` values (null, 'signup_header', 'action');
insert into `wp_typing` values (null, 'signup_hidden_fields', 'action');
insert into `wp_typing` values (null, 'spam_comment', 'action');
insert into `wp_typing` values (null, 'spammed_comment', 'action');
insert into `wp_typing` values (null, 'start_previewing_theme', 'action');
insert into `wp_typing` values (null, 'stop_previewing_theme', 'action');
insert into `wp_typing` values (null, 'submitlink_box', 'action');
insert into `wp_typing` values (null, 'submitpage_box', 'action');
insert into `wp_typing` values (null, 'submitpost_box', 'action');
insert into `wp_typing` values (null, 'switch_blog', 'action');
insert into `wp_typing` values (null, 'switch_theme', 'action');
insert into `wp_typing` values (null, 'template_redirect', 'action');
insert into `wp_typing` values (null, 'the_widget', 'action');
insert into `wp_typing` values (null, 'tool_box', 'action');
insert into `wp_typing` values (null, 'trackback_post', 'action');
insert into `wp_typing` values (null, 'transition_comment_status', 'action');
insert into `wp_typing` values (null, 'transition_post_status', 'action');
insert into `wp_typing` values (null, 'trash_comment', 'action');
insert into `wp_typing` values (null, 'trash_post_comments', 'action');
insert into `wp_typing` values (null, 'trashed_comment', 'action');
insert into `wp_typing` values (null, 'trashed_post', 'action');
insert into `wp_typing` values (null, 'trashed_post_comments', 'action');
insert into `wp_typing` values (null, 'unarchive_blog', 'action');
insert into `wp_typing` values (null, 'unload_textdomain', 'action');
insert into `wp_typing` values (null, 'unmature_blog', 'action');
insert into `wp_typing` values (null, 'unspam_comment', 'action');
insert into `wp_typing` values (null, 'unspammed_comment', 'action');
insert into `wp_typing` values (null, 'untrash_comment', 'action');
insert into `wp_typing` values (null, 'untrash_post', 'action');
insert into `wp_typing` values (null, 'untrash_post_comments', 'action');
insert into `wp_typing` values (null, 'untrashed_comment', 'action');
insert into `wp_typing` values (null, 'untrashed_post', 'action');
insert into `wp_typing` values (null, 'untrashed_post_comments', 'action');
insert into `wp_typing` values (null, 'update_blog_public', 'action');
insert into `wp_typing` values (null, 'update_option', 'action');
insert into `wp_typing` values (null, 'update_option_{$option}', 'action');
insert into `wp_typing` values (null, 'update_postmeta', 'action');
insert into `wp_typing` values (null, 'update_site_option', 'action');
insert into `wp_typing` values (null, 'update_site_option_{$option}', 'action');
insert into `wp_typing` values (null, 'update_usermeta', 'action');
insert into `wp_typing` values (null, 'update_wpmu_options', 'action');
insert into `wp_typing` values (null, 'update_{$meta_type}_meta', 'action');
insert into `wp_typing` values (null, 'updated_option', 'action');
insert into `wp_typing` values (null, 'updated_postmeta', 'action');
insert into `wp_typing` values (null, 'updated_usermeta', 'action');
insert into `wp_typing` values (null, 'updated_{$meta_type}_meta', 'action');
insert into `wp_typing` values (null, 'upload_ui_over_quota', 'action');
insert into `wp_typing` values (null, 'user_admin_menu', 'action');
insert into `wp_typing` values (null, 'user_admin_notices', 'action');
insert into `wp_typing` values (null, 'user_edit_form_tag', 'action');
insert into `wp_typing` values (null, 'user_new_form_tag', 'action');
insert into `wp_typing` values (null, 'user_register', 'action');
insert into `wp_typing` values (null, 'validate_password_reset', 'action');
insert into `wp_typing` values (null, 'welcome_panel', 'action');
insert into `wp_typing` values (null, 'widgets.php', 'action');
insert into `wp_typing` values (null, 'widgets_admin_page', 'action');
insert into `wp_typing` values (null, 'widgets_init', 'action');
insert into `wp_typing` values (null, 'wp-mail.php', 'action');
insert into `wp_typing` values (null, 'wp_after_admin_bar_render', 'action');
insert into `wp_typing` values (null, 'wp_before_admin_bar_render', 'action');
insert into `wp_typing` values (null, 'wp_blacklist_check', 'action');
insert into `wp_typing` values (null, 'wp_create_file_in_uploads', 'action');
insert into `wp_typing` values (null, 'wp_create_nav_menu', 'action');
insert into `wp_typing` values (null, 'wp_dashboard_setup', 'action');
insert into `wp_typing` values (null, 'wp_delete_nav_menu', 'action');
insert into `wp_typing` values (null, 'wp_delete_post_revision', 'action');
insert into `wp_typing` values (null, 'wp_enqueue_media', 'action');
insert into `wp_typing` values (null, 'wp_enqueue_scripts', 'action');
insert into `wp_typing` values (null, 'wp_footer', 'action');
insert into `wp_typing` values (null, 'wp_head', 'action');
insert into `wp_typing` values (null, 'wp_insert_comment', 'action');
insert into `wp_typing` values (null, 'wp_insert_post', 'action');
insert into `wp_typing` values (null, 'wp_loaded', 'action');
insert into `wp_typing` values (null, 'wp_login', 'action');
insert into `wp_typing` values (null, 'wp_login_failed', 'action');
insert into `wp_typing` values (null, 'wp_logout', 'action');
insert into `wp_typing` values (null, 'wp_meta', 'action');
insert into `wp_typing` values (null, 'wp_network_dashboard_setup', 'action');
insert into `wp_typing` values (null, 'wp_print_footer_scripts', 'action');
insert into `wp_typing` values (null, 'wp_print_scripts', 'action');
insert into `wp_typing` values (null, 'wp_print_styles', 'action');
insert into `wp_typing` values (null, 'wp_register_sidebar_widget', 'action');
insert into `wp_typing` values (null, 'wp_restore_post_revision', 'action');
insert into `wp_typing` values (null, 'wp_set_comment_status', 'action');
insert into `wp_typing` values (null, 'wp_trash_post', 'action');
insert into `wp_typing` values (null, 'wp_unregister_sidebar_widget', 'action');
insert into `wp_typing` values (null, 'wp_update_comment_count', 'action');
insert into `wp_typing` values (null, 'wp_update_nav_menu', 'action');
insert into `wp_typing` values (null, 'wp_update_nav_menu_item', 'action');
insert into `wp_typing` values (null, 'wp_user_dashboard_setup', 'action');
insert into `wp_typing` values (null, 'wpmu_activate_blog', 'action');
insert into `wp_typing` values (null, 'wpmu_activate_user', 'action');
insert into `wp_typing` values (null, 'wpmu_blog_updated', 'action');
insert into `wp_typing` values (null, 'wpmu_delete_user', 'action');
insert into `wp_typing` values (null, 'wpmu_new_blog', 'action');
insert into `wp_typing` values (null, 'wpmu_new_user', 'action');
insert into `wp_typing` values (null, 'wpmu_options', 'action');
insert into `wp_typing` values (null, 'wpmu_update_blog_options', 'action');
insert into `wp_typing` values (null, 'wpmu_upgrade_page', 'action');
insert into `wp_typing` values (null, 'wpmu_upgrade_site', 'action');
insert into `wp_typing` values (null, 'wpmuadminedit', 'action');
insert into `wp_typing` values (null, 'wpmuadminresult', 'action');
insert into `wp_typing` values (null, 'wpmublogsaction', 'action');
insert into `wp_typing` values (null, 'wpmueditblogaction', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_blogger_deletePost', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_blogger_editPost', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_blogger_newPost', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_mw_editPost', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_mw_newMediaObject', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_mw_newPost', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_wp_deleteCategory', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_wp_deleteComment', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_wp_deletePage', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_wp_editComment', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_wp_newCategory', 'action');
insert into `wp_typing` values (null, 'xmlrpc_call_success_wp_newComment', 'action');
insert into `wp_typing` values (null, 'xmlrpc_publish_post', 'action');
insert into `wp_typing` values (null, 'xmlrpc_rsd_apis', 'action');
insert into `wp_typing` values (null, '{$new_status}_{$post->post_type}', 'action');
insert into `wp_typing` values (null, '{$old_status}_to_{$new_status}', 'action');

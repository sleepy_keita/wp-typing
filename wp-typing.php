<?php
/*
Plugin Name: wp-typing Game
*/

define('WP_TYPING_PATH', 'typing');
define('WP_TYPING_URL', plugins_url('', __FILE__));
define('WP_TYPING_DIR', dirname(__FILE__));
define('WP_TYPING_TABLE', 'typing');

register_activation_hook(__FILE__, 'wp_typing_activation');
register_deactivation_hook(__FILE__, 'wp_typing_deactivation');

function wp_typing_activation() {
    add_rewrite_endpoint(WP_TYPING_PATH, EP_ROOT);
    flush_rewrite_rules();

    global $wpdb;
    $table = $wpdb->prefix.WP_TYPING_TABLE;
    if ($wpdb->get_var("show tables like '$table'") !== $table) {
        $sql = "CREATE TABLE `{$table}` (
            `id` bigint(20) unsigned not null auto_increment,
            `word` varchar(255),
            `cat` varchar(255),
            primary key (`id`),
            key `cat` (`cat`)
        );";
        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);
    }
}

function wp_typing_deactivation() {
    flush_rewrite_rules();
}




new WP_Typing(WP_TYPING_PATH); // クラスを実行！

class WP_Typing{ // クラス名は適当に

function __construct($endpoint)
{
    $this->endpoint = $endpoint;
    add_action('plugins_loaded', array(&$this, "plugins_loaded"));
}

public function plugins_loaded()
{
    add_action('init', array(&$this, 'init'));
    add_filter('query_vars', array(&$this, 'query_vars'));
    add_action('template_redirect', array(&$this, 'template_redirect'));
}

public function init()
{
    if (is_admin()) {
        global $pagenow;
        if ($pagenow === 'plugins.php') {
            if (isset($_GET['plugin'])) {
                if (basename(__FILE__) === basename($_GET['plugin'])) {
                    return; // なにもしない
                }
            }
        }
        add_rewrite_endpoint($this->endpoint, EP_ROOT);
    }
}

public function query_vars($vars)
{
    $vars[] = $this->endpoint;
    return $vars;
}

public function template_redirect()
{
    global $wp_query;
    if (isset($wp_query->query[$this->endpoint])) {
        nocache_headers();

        if (get_query_var($this->endpoint) === 'json') {
            require_once(dirname(__FILE__).'/app/json.php');
            exit;
        } elseif (get_query_var($this->endpoint)) {
            $wp_query->set_404();
            status_header(404);
            return;
        } else {
            require_once(dirname(__FILE__).'/app/index.php');
            exit;
        }
    }
}

}

// EOF

(function($, window, document, undefined){
  var ok_type, miss_type, word_index, text_index, sleep, sec, start_time, end_time, timer, texts = [], label, codex, display, init, wait, start_game, set_text, end_game;

  label = {
    'function': 'WordPress Function',
    'action': 'Action Hook',
    'filter': 'Filter Hook',
  };

  codex = {
    'function': 'http://codex.wordpress.org/Function_Reference/',
    'action': 'http://codex.wordpress.org/Plugin_API/Action_Reference/',
    'filter': 'http://codex.wordpress.org/Plugin_API/Filter_Reference/',
  };

  display = function(){
    ok_type = 0;
    miss_type = 0;
    word_index = 0;
    text_index = 0;
    sleep = 3;
    $('#word').text('');
    $('#cat').text('');

    $.getJSON(
      window.typingEndpoint,
      function(data){
        texts = data;
        $('#word').html('<a id="btn-start" href="javascript:void(0);" class="btn btn-success btn-large">Press Space to start</a>');
        $(window).unbind('keypress');
        $(window).keypress(init);
        $('#btn-start').click(init);
      }
    );
  }

  init = function (e) {
    if (e.charCode === 32 || ($(this).attr('id') === 'btn-start')) {
      $(window).unbind('keypress', init);
      timer = setInterval(wait, 1000);
    }
  }

  wait = function (){
    if (sleep === 0) {
      start_game();
      clearInterval(timer);
      return;
    }
    $('#word').html('<span style="color:#ff0000;">'+sleep+'</span>');
    sleep = sleep - 1;
  }

  start_game = function(){
    set_text();
    start_time = new Date();

    $(window).keypress(function(e){
      var str = String.fromCharCode(e.charCode);
      if (str === texts[word_index].word.substr(text_index, 1)) {
        ok_type = ok_type + 1;
        $('#word-'+text_index).attr('class', 'text-ok');
        text_index = text_index + 1;
        if (text_index === texts[word_index].word.length) {
          word_index = word_index + 1;
          text_index = 0;
          if (word_index === texts.length) {
            end_game();
          } else {
            set_text();
          }
        }
      } else {
        $('#word-'+text_index).css('color', '#ff0000');
        $('#box').animate({backgroundColor: '#ffffcc'}, 100, function(){
          $(this).animate({backgroundColor: '#f5f5f5'});
        });
        miss_type = miss_type + 1;
      }
    });
  }

  set_text = function(){
    var str = texts[word_index].word;
    $('#word').html('');
    $('#cat').text(label[texts[word_index].cat]);
    for (var i=0; i<str.length; i++) {
      $('#word').append('<span id="word-'+i+'">'+str.substr(i, 1)+'</span>');
    }
  }

  end_game = function(){
    var time, wpm, cpm, acc, url, text, i;
    end_time = new Date();

    time = Math.ceil((end_time - start_time) / 1000);
    cpm = Math.ceil(ok_type / time * 60);
    wpm = cpm / 5;
    acc = Math.ceil((1 - (miss_type / (ok_type + miss_type))) * 100);

    $('#time').text(time + ' sec');
    $('#cpm').text(cpm);
    $('#wpm').text(Math.ceil(wpm));
    $('#acc').text(acc + ' %');

    $('#refs').html('');
    for (i=0; i<texts.length; i++) {
      $('#refs').append('<li><a href="'+codex[texts[i].cat]+texts[i].word+'" target="_blank">'+texts[i].word+'</a></li>');
    }

    url = 'https://twitter.com/intent/tweet?url=http://firegoby.jp/typing/&text=';
    text = encodeURIComponent("My score is "+cpm+' and Accuracy is '+acc+'% #wptyping');
    $('#tweet').attr('href', url+text);

    $(window).unbind('keypress');
    $('#myModal').modal();
  }

  display();

  $('#myModal').on('hidden', function(){
    display();
  });

  // Prevent the backspace key from navigating back.
  $(document).unbind('keydown').bind('keydown', function (e) {
    var doPrevent = false;
    if (e.keyCode === 8) {
      var d = e.srcElement || e.target;
      if ((d.tagName.toUpperCase() === 'INPUT' && (d.type.toUpperCase() === 'TEXT' || d.type.toUpperCase() === 'PASSWORD' || d.type.toUpperCase() === 'FILE'))
           || d.tagName.toUpperCase() === 'TEXTAREA') {
          doPrevent = d.readOnly || d.disabled;
      }
      else {
          doPrevent = true;
      }
    }

    if (doPrevent) {
      e.preventDefault();
    }
  });
})(jQuery, window, document);

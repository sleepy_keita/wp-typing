<!DOCTYPE HTML>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>WordPress Typing Game</title>
    <link href="<?php echo WP_TYPING_URL; ?>/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<style type="text/css">
body {
    padding-top: 20px;
    padding-bottom: 40px;
    color: #555555;
}
.container-narrow {
    margin: 0 auto;
    max-width: 800px;
}
.container-narrow > hr {
    margin: 30px 0;
}
#box
{
    padding-top: 100px;
    padding-bottom: 80px;
}
#word
{
    text-align: center;
    font-size: 30px;
    color: #999999;
    font-weight: bolder;
}
#cat
{
    margin-top: 40px;
    text-align: center;
    color: #999999;
}
#word span.text-ok
{
    color: orange !important;
}
</style>
<script type="text/javascript">
  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-1434500-8']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();
</script>
</head>
<body>
<div class="container-narrow">

      <div class="masthead">
        <ul class="nav nav-pills pull-right">
          <li><a href="http://firegoby.jp/">About</a></li>
        </ul>
        <h3 class="muted">WordPress Typing Game</h3>
      </div>
<?php if (isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) && preg_match('/^ja/i', $_SERVER['HTTP_ACCEPT_LANGUAGE'])): ?>
    <p>WordPress のテンプレートタグと、フィルターフック、アクションフックだけが出現するタイピングゲームです。<br>毎日やればタイピングと同時に WordPress のテンプレートタグも覚えられるかもよ！</p>
    <p>単語は10個ランダムに表示されます。最後にスコアも出ますよ！</p>
<?php else: ?>
<p>Learn WordPress Tempate Tags, Filter Hooks, and Action Hooks while polishing up your typing skill.</p>
<p>Find out your score after typing 10 random tags/hooks!</p>
<?php endif; ?>

<div style="25px;"><a href="https://twitter.com/share" class="twitter-share-button" data-lang="ja">ツイート</a>
<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script></div>

<div style="margin-bottom:1em; height:24px;" class="fb-like" data-href="http://firegoby.jp/typing/" data-send="false" data-width="450" data-show-faces="false"></div>

    <div id="box" class="well">
        <div id="word"></div>
        <div id="cat"></div>
    </div>

</div>

<div id="myModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
    <h3 id="myModalLabel">Mission Complete !</h3>
  </div>
  <div class="modal-body">
<div class="row-fluid">
<div class="span6">
<table class="table table-bordered table-striped">
    <tr>
        <th>Time</th>
        <td style="text-align:right;" id="time"></td>
    </tr>
    <tr>
        <th>CPM</th>
        <td style="text-align:right;" id="cpm"></td>
    </tr>
    <tr>
        <th>WPM</th>
        <td style="text-align:right;" id="wpm"></td>
    </tr>
    <tr>
        <th>Accuracy</th>
        <td style="text-align:right;" id="acc"></td>
    </tr>
</table>
</div>
<div class="span6"><ol id="refs"></ol></div>
</div>
  </div>
  <div class="modal-footer">
    <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
    <a class="btn btn-primary" id="tweet" target="_blank">Tweet</a>
  </div>
</div>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
<script src="<?php echo WP_TYPING_URL; ?>/jquery.color.min.js"></script>
<script src="<?php echo WP_TYPING_URL; ?>/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript">
  window.typingEndpoint = '<?php echo site_url('/' . WP_TYPING_PATH); ?>/json/';
</script>
<script type="text/javascript" src="<?php echo WP_TYPING_URL; ?>/script.min.js?ver=<?php echo filemtime(WP_TYPING_DIR.'/script.js'); ?>"></script>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/ja_JP/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</body>
</html>

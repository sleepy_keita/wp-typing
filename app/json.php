<?php

global $wpdb;

$table = $wpdb->prefix.WP_TYPING_TABLE;
$sql = 'SELECT `word`, `cat` FROM `' . $table . '` ORDER BY RAND() LIMIT 0, 10';
$words = $wpdb->get_results($sql);

header('Content-type: application/json; charset=utf-8');
echo json_encode($words);
exit;
